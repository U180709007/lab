package shapes2d;

public class Square {
	private double side;
	public Square() {
		this.side=1.0;
	}
	public Square(double side) {
		this.side=side;
	}
	public double getSide() {
		return this.side;
	}
	public void setSide(double side) {
		this.side=side;
	}
	public double area() {
		return side*side;
	}
	public String toString() {
		return "side= "+this.side;
	}
	
}
