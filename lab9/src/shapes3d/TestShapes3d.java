package shapes3d;

public class TestShapes3d {

	public static void main(String[] args) {
		Cylinder g=new Cylinder();
		Cube h=new Cube();
		Cylinder c=new Cylinder(8,15);
		Cube b=new Cube(5);
		System.out.println("First cube area: "+h.area());
		System.out.println("First cube volume: "+h.volume());
		System.out.println("Second cube area: "+b.area());
		System.out.println("Second cube volume: "+b.volume());
		System.out.println("First cylinder area: "+g.area());
		System.out.println("First cylinder volume: "+g.volume());
		System.out.println("Second cylinder area: "+c.area());
		System.out.println("Second cylinder volume: "+c.volume());
		System.out.println("first cube: "+h.toString());
		System.out.println("second cube "+b.toString());
		System.out.println("first cylinder: "+g.toString());
		System.out.println("second cylinder "+c.toString());
		
		

	}

}
