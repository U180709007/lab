package demo;

//import stack.Stack;
import stack.StackArrayImpl;
//import stack.StackImpl;

public class StackDemo {
	
	public static void main(String[] args) {
		StackArrayImpl stack = new StackArrayImpl();
		stack.push("A");
		stack.push("B");
		stack.push("C");
		stack.push("D");
		while (!stack.empty()){
			System.out.println(stack.pop());
		}
	}

}
