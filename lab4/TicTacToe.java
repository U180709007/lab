


import java.io.IOException;
import java.util.Scanner;

public class TicTacToe {

    public static void main(String[] args) throws IOException {
        Scanner reader = new Scanner(System.in);
        char[][] board = {
            {
                ' ',
                ' ',
                ' '
            },
            {
                ' ',
                ' ',
                ' '
            },
            {
                ' ',
                ' ',
                ' '
            }
        };
        
        printBoard(board);
        int colLast=0,rowLast=0;
        int movecount = 0;
        while (true) {
            int row, col;
            do {
                System.out.print("Player 1 enter row number:");
                row = reader.nextInt();

                System.out.print("Player 1 enter column number:");
                col = reader.nextInt();

            }while (!(row < 4 && row > 0 && col < 4 && col > 0 && board[row - 1][col - 1] == ' ')); 
            	                                                                                      
                board[row - 1][col - 1] = 'X';
                printBoard(board);
                movecount++;
                if ((checkBoard(board,row-1,col-1))) { 
                        System.out.println("player 1 wins");
                        break;
                    
                }
                
            
             do {

                System.out.print("Player 2 enter row number:");
                row = reader.nextInt();
                System.out.print("Player 2 enter column number:");
                col = reader.nextInt();
            }while (!(row < 4 && row > 0 && col < 4 && col > 0 && board[row - 1][col - 1] == ' ')); 
                    //tekrar soracak
                board[row - 1][col - 1] = 'O';
                printBoard(board);
                movecount++;//geçerli hamleleri sayıyor
                if ((checkBoard(board,row-1,col-1))) { //fonksiyon true dönerse kazananı ilan edip while döngüsünü kırmalı.
                		System.out.println("player 2 wins");
                		break;
                }
                
            
                if (movecount==9) {//kazanan olmazsa berabere diyip while döngüsünü kırmalı
                	System.out.println("It's a draw!!");
                	break;
                }
        	}
        	
            reader.close();
        }
	
    public static void printBoard(char[][] board) {
        System.out.println("    1   2   3");
        System.out.println("   -----------");
        for (int row = 0; row < 3; ++row) {
            System.out.print(row + 1 + " ");
            for (int col = 0; col < 3; ++col) {
                System.out.print("|");
                System.out.print(" " + board[row][col] + " ");
                if (col == 2)
                    System.out.print("|");
            }
            System.out.println();
            System.out.println("   -----------");
        }

    }

    public static boolean checkBoard(char[][]board, int rowLast , int colLast) {
        char s = board[rowLast][colLast];

        
        boolean flag = true;
        for (int col = 0; col< 3; col++) {
            if (board[rowLast][col] != s ) {
                flag = false;
                break;
            }
        }
        if (flag) {
            return true;
        }
        
        flag = true;
        for (int row =0; row<3; row++) {
            if(board[row][colLast] != s) {
                flag = false;
                break;
            }
        }
        if (flag) {
            return true;
        }

        flag = true;
        if (rowLast==colLast) {
            //flag = true;
            for (int l=0; l<3; l++) {
                if (board[l][l] != s) {
                    flag = false;
                    break;
                }
            }
            if (flag) {
                return true;
            }
        }
        flag = true;

        
                if (rowLast+colLast==2) {
                    for (int row = 0; row<3; row++) {
                        if (board[row][2-row] != s ) {
                            flag = false;
                            break;
                        }
                    }
                    if (flag) {
                        return true;
                    }
                }
        return false;
    }
    
}
